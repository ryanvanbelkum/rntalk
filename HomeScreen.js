import React, {Component} from 'react';
import { Button, View } from 'react-native';

export default class HomeScreen extends Component {
  render() {
      const { navigate } = this.props.navigation;
      return (
          <View>
              <Button
                  title="Spinning image"
                  onPress={() =>
                      navigate('Animation')
                  }
              />
              <Button
                  title="Flatlist"
                  onPress={() =>
                      navigate('Lists')
                  }
              />
          </View>
      );
  }
}