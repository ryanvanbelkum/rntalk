import {
    createStackNavigator,
} from 'react-navigation';
import HomeScreen from './HomeScreen';
import Animation from './Animation';
import Lists from './Lists';

const Navigator = createStackNavigator({
    Home: { screen: HomeScreen },
    Animation: { screen: Animation },
    Lists: { screen: Lists },
});

export default Navigator;