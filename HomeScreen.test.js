import React from 'react';
import HomeScreen from './HomeScreen';
import { shallow } from 'enzyme';

describe('HomeScreen', () => {
    let props;
    let wrapper;

    beforeEach(() => {
        props = {
            navigation: {
               navigate: jest.fn()
            }
        };
        wrapper = shallow(<HomeScreen {...props} />);
    });

    it('successfully renders component', () => {
        expect(wrapper).toMatchSnapshot();
    });

    it('navigate to animation', () => {
        wrapper.find('Button[title="Spinning image"]').simulate('press');
        expect(props.navigation.navigate).toHaveBeenCalledWith('Animation');
    });

    it('navigate to list', () => {
        wrapper.find('Button[title="Flatlist"]').simulate('press');
        expect(props.navigation.navigate).toHaveBeenCalledWith('Lists');
    });
});
