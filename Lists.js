import React from 'react';
import {
    Text,
    FlatList
} from 'react-native'

const data = [
    { key: 1, name: 'John' },
    { key: 2, name: 'Greg' },
    { key: 3, name: 'Jim' },
    { key: 4, name: 'Fred' },
    { key: 5, name: 'Justin' },
    { key: 6, name: 'Ralph' },
];

const keyExtractor = (item) => String(item.key);

const renderItem = ({item}) => <Text>{item.name}</Text>;

const List = () => (
            <FlatList
                data={data}
                keyExtractor={keyExtractor}
                renderItem={renderItem}
            />
        );

export default List;